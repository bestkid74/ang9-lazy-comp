import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NQueensComponent } from './n-queens/n-queens.component';

const routes: Routes = [
  {
    path: 'n-queens',
    component: NQueensComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
