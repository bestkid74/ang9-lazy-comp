/// <reference lib="webworker" />
import nQueens from './n-queens';

addEventListener('message', ({ data }) => {
  const result = nQueens(data.count);
  postMessage(result, undefined);

  // const response = `worker response to ${data}`;
  // postMessage(response);
});
